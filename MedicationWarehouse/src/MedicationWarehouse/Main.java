package MedicationWarehouse;

import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import MedicationWarehouse.Admin.Admin;
import MedicationWarehouse.model.Dori;
import MedicationWarehouse.repository.GeneralRepository;

import java.io.BufferedReader;
import java.util.*;

public class Main {
    public static void main(String[] args) {
    	boolean shouldStop = true;
        Scanner scanner = new Scanner(System.in);
        String input;
        Dorilar dorilar = new Dorilar();
        Admin admin = new Admin();
        GeneralRepository generalrepository = new GeneralRepository();
        Dori dori1 = new  Dori(" "," "," "," ");
        while (true) {
            System.out.println("Dasturga kirish uchun rolni kiriting");
            System.out.println("🔵User");
            System.out.println("🔵Admin");
            System.out.println("Kiriting⬇️");
            input = scanner.nextLine();
            switch(input) {
            case "user":
            case "User":{
                System.out.println("Siz User rolidasiz✅\n\n");
                System.out.println("Sizning imkoniyatlaringiz⬇️");
                System.out.println("🔵Qidirish");
                System.out.println("🔵Dorilar");
                System.out.println();
                System.out.println("Davom etishingiz uchun imkoniyatlaringizdan birini kiriting⬇️");
                String t = scanner.nextLine();
                
                switch(t) {
                case "qidirish":
                case "Qidirish":{
                	 System.out.println("Qidirmoqchi bo'lgan dorini nomini kiriting");
                	   String smm = scanner.nextLine();
                	   admin.INomi();
                       
                       List<String> result = generalrepository.qidirish(smm);
                       if (result != null) {
                           System.out.println(result);
                       }  
                       else {
                    	   System.out.println("Xato kiritdingiz");
                       }
                	}break;
                case "dorilar":
                case "Dorilar":{
                    System.out.println("Sizning imkoniyatlaringiz⬇️");
                    System.out.println();
                    System.out.println("🔵Dorilar ro'yhati");
                    System.out.println("🔵Dorilar soni");
                  
                     String s = scanner.nextLine();
                    switch (s) {
                    case "Dorilar ro'yhati":
                    case "dorilar ro'yhati": {
                  System.out.println(generalrepository.royhat());
                    }
                    break;
                    case "Dorilar soni":
                    case "dorilar soni":{
                    	 System.out.println("soni: "+generalrepository.soni());	                  
                    
                    }
                    break;
                          }
                      	}break;
                default:    System.out.println("Xato kiritildi");break;
                
                }
              
                  };break;
            case "admin","Admin":{
            	  System.out.println("Admin parolini kiriting⬇️");
                  String p = scanner.nextLine();
                  if (p.equals(admin.parol)) {
                      System.out.println("Siz admin rolidasiz");
                      System.out.println();
                      System.out.println("Sizning imkoniyatlaringiz⬇️");
                      System.out.println();
                      System.out.println("🔵Dori qo'shish");
                      System.out.println("🔵Dorini o'zgartirish");
                      System.out.println("🔵Dorini o'chirish");
                      System.out.println("🔵Parolni o'zgartirish");
                      String w = scanner.nextLine();
                      switch (w) {
                      case "Dori qo'shish","dori qo'shish":{
                    	  System.out.println("Yangi dorini malumotlarini kiriting⬇️");
                    	  System.out.println("Nomini kiriting");
                          String name = new Scanner(System.in).nextLine();
                          System.out.println("Muddatini kiriting");
                          String muddati = new Scanner(System.in).nextLine();
                          System.out.println("Ishlab chiqarilgan sanasini kiriting");
                          String sana = new Scanner(System.in).nextLine();
                          System.out.println("Summasini kiriting");
                          String summa = new Scanner(System.in).nextLine();
                          
                          dori1.nomi= name;
                          dori1.muddat = muddati;
                          dori1.sana = sana;
                          dori1.summa = summa;
                          List<String> qoshilgan = admin.qoshish(dori1);
                          System.out.println(qoshilgan);
                          break;
                      }
                      
                      case "Dorini o'zgartirish","dorini o'zgartirish":{
                    	  System.out.println("O'zgartirilishi kerak bo'lgan dori nomini yozing");
                          String name = new Scanner(System.in).next();
                          System.out.println("Muddatini kiriting");
                          String muddati = new Scanner(System.in).next();
                          System.out.println("Ishlab chiqarilgan sanasini kiriting");
                          String sana = new Scanner(System.in).next();
                          System.out.println("Summasini kiriting");
                          String summa = new Scanner(System.in).next();
                          Dori dori1 ;
                          dori1.nomi= name;
                          dori1.muddat = muddati;
                          dori1.sana = sana;
                          dori1.summa = summa;
                          admin.yangilashNomi(dori1);
                      }
                      break;
                      case "Dorini o'chirish","dorini o'chirish":
                      {
                    	  System.out.print("O'chiriladigan dorini nomini kiriting");
                    	  String name = new Scanner(System.in).nextLine();
                    	  Dori dori1 ;
                          dori1.nomi= name;
                       	  admin.ochirish(dori1);
                    	  
                    	      }
                      break;
                      case "Parolni o'zgartirish","parolni o'zgartirish":{
                    	  System.out.println("Yangi admin parolini kiriting⬇️");
                          String parol = scanner.nextLine();
                          admin.parolozgartirish(parol);
                          System.out.println("Sizning yangi parolingiz: " +parol);
                      }
                      break;
                      
                      }
                  } else {
                      System.out.println("Parol xato kiritildi");
                  }
            }; break;
            default:    System.out.println("Xato kiritildi");break;
            }

        }
    }
    
}
