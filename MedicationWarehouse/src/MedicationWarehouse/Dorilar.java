package MedicationWarehouse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Dorilar 
{
	    private List<String> nomi;
	    private List<String> muddati;
	    private List<String> summa;
	    private  List<String> ishlabChiqarilganJoyi;
	    private Map<String, List<String>> malumot = new HashMap<>();

    public Dorilar() {
         this.nomi = new ArrayList<String>();
        this.muddati = new ArrayList<String>();
        this.summa = new ArrayList<String>();
        this.ishlabChiqarilganJoyi = new ArrayList<String>();
        malumot.put("Parasetamol", Arrays.asList("Parasetamol","2023.05.30","1400","tosh.4254"));
        malumot.put("Ritalin", Arrays.asList("Ritalin","2023.05.30","1500","ferg.dssd"));
        malumot.put("Aspiring", Arrays.asList("Aspirin","2020.03.30","1400","tosh.4254"));
    }

    public void INomi() {
        for (int i = 0; i < nomi.size(); i++) {
            List<String> values = new ArrayList<>();
            values.add(nomi.get(i));
            values.add(muddati.get(i));
            values.add(summa.get(i));
            values.add(ishlabChiqarilganJoyi.get(i));
            malumot.put(nomi.get(i), values);
        }
    }
    public int soni() {
      return nomi.size();
    }
    public List<String>  royhat() {
    	List<String> values = new ArrayList<>();
        malumot.forEach((k, v) -> {
        	values.addAll(v);
        });
           return values;
    }

    public List<String> qidirish(String nomi) {
        if (malumot.containsKey(nomi)) {
            return malumot.get(nomi);
        } else {
            System.out.println("Xato kiritildi!!");
            return null;
        }
    }
    public void ochirish(String nomi) {
    	String canI = null;
    	malumot.remove(nomi);    	
    	  malumot.forEach((k, v) -> {
         	System.out.println(v);
         });
    	    }
    public void yangilashNomi(String nomi, String muddat, String Ishlab, String summ) {
        List<String> values = malumot.get(nomi);
        values.set(1, muddat);
        values.set(2, Ishlab);
        values.set(3, summ);
        malumot.put(nomi, values);
        malumot.forEach((k, v) -> {
         	System.out.println(v);
         });
    }
    public List<String> qoshish(String nomi, String muddat, String ishlab, String summ) {
    	this.nomi.add(nomi);
    	this.muddati.add(muddat);
    	this.ishlabChiqarilganJoyi.add(ishlab);
    	this.summa.add(summ);
    	this.malumot.put(nomi, Arrays.asList(nomi,muddat,ishlab,summ));
    	return Arrays.asList(nomi,muddat,ishlab,summ);
    }
}
